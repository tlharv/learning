#include <stdio.h>
#include <stdlib.h>
#include <time.h>





int sum_dice(int dice_faces, int num_of_dice)
{
	srand((unsigned)time(NULL)); // seed for random number generator

	int roll;
	int sum=0;
	for (int i=0; i<num_of_dice; i++)
	{
		roll = (rand() % dice_faces) + 1;
		//printf("I rolled a %d\n",roll);
		sum += roll;
	}
	return(sum);
}




void main()
{
	int dice_faces, num_of_dice, roll;
	int sum = 0;
	
	printf("Enter the number of dice faces:\n");
	scanf("%d", &dice_faces);
	printf("Enter the number of d%d to roll:\n",dice_faces);
	scanf("%d", &num_of_dice);
	printf("Rolling %d d%d\n",num_of_dice,dice_faces);
	
	sum = sum_dice(dice_faces, num_of_dice);
	printf("The sum of rolling %d d%d is %d.\n",num_of_dice, dice_faces, sum);
}
