#include <stdio.h>  // for the basics
#include <string.h> // for converting int to string
#include <time.h>   // for random number generator seed
#include <stdlib.h> // for random number function

struct Dog
{
	char name[10];
	char breed[10];
	int age;
};



void main()
{
	// Create a variable of type Dog
	struct Dog Rocket;
	strcpy(Rocket.breed,"Red Heeler"); // Must use strcpy - you can't just say Rocket.breed = "Red Heeler."
	Rocket.age = 3;

	// Create a pointer of type Dog and point it to Rocket's address
	struct Dog *ptr_to_Rocket = &Rocket;

	// Show the dot notation for values of the Dog type when the variable is referenced directly.
	printf("Rocket is a %s and is %d years old.\n",Rocket.breed,Rocket.age);

	// Show the arrow notation for values in the Dog type when the pointer to Rocket's variable address is referenced
	printf("Rocket is a %s and is %d years old.\n", ptr_to_Rocket->breed, ptr_to_Rocket->age);

	// Now play around with creating instances of this structure in an array.
	int dog_count;
	printf("How many dogs? ");
	scanf("%d", &dog_count);
	printf("Ok, I'm going to make %d dogs.\n",dog_count);

	// Now crank out dogs
	struct Dog pooch[dog_count];  // Create the array of dogs
	int max_dog_age = 15;
	int this_dog_age = 0;

	for (int i=0; i<dog_count; i++)
	{
		char dogname[15];
		sprintf(dogname,"Dog_%d",i);
		strcpy(pooch[i].name, dogname);
		strcpy(pooch[i].breed, "Basic Mutt");

		// give it a random age between 1 and max_dog_age
		//time_t t;
		srand((unsigned) time(NULL)); // seed for RNG
		pooch[i].age = (rand() % max_dog_age) + 1;
	}

	// Let's see if we got them all.
	for (int j=0; j<dog_count; j++)
	{
		printf("%s is a %s and is %d years old.\n",pooch[j].name,pooch[j].breed,pooch[j].age);
	}

}
