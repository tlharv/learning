#include <stdio.h>
#include <string.h>

#define num_of_strings 4
#define len_of_strings 20

char arr[num_of_strings][len_of_strings] =
{
	"ABC", 
	"DEF", 
	"GHI", 
	"JKL"
};

for (int i=0; i<num_of_strings; i++)
{
	printf("'%s' has length %d\n", arr[i], strlen[arr[i]);
}
