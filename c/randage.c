#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int sum_dice(int dice_faces)
{
	srand((unsigned)time(NULL)); // seed for random number generator

	int roll=0;
	roll = (rand() % dice_faces) + 1;
	return(roll);
}




void main()
{
	int dice_faces, roll;
	printf("Enter the number of dice faces:\n");
	scanf("%d", &dice_faces);

	for (int x=0; x<100; x++)
	{
		roll = 0;
		printf("You rolled a %d\n",sum_dice(dice_faces));
	}
}
