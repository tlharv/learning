#include <stdio.h>

// Pointers are one of the ways C allows (or sometimes forces) you to think about what the actual hardware of your
// computer is doing.  A good understanding of pointers gives you a good understanding of how the compiler handles memory.

void main(void)
{
	int a;		// This just sets aside memory space for an integer
	int *ptr_to_a;	// This just sets aside memory space for a pointer

	ptr_to_a = &a;	// Now we're saying that ptr_to_a equals the ADDRESS of variable a (the ampersand means address).
	a = 5;		// This sets the value of the variable a

	printf("The value of a is %d\n",a);	// Straightforward. Show the value of the integer variable a.

	// If we reference *ptr_to_a anywhere in the program, it'll point us to the value of a
	// (or, more specifically, it routes us to the address of a and tells us the value stored there.)

	// Once a pointer exists (was declared) using the asterisk means "the variable pointed to by this pointer."
	printf("The value of *ptr_to_a is %d\n",*ptr_to_a);	// Exact same result as showing value of a. Just goes about it through a's address.

	// Once a pointer exists (was declared) using the asterisk means "the variable pointed to by this pointer."
	*ptr_to_a = 6; // This is another way of setting a = 6.
	printf("The value of a is now %d\n",a);

	// And so you can see the difference between what the variables store.
	// Some store the value of a, and some store the address of a.

	printf("The value of ptr_to_a is %d\n", ptr_to_a);
	printf("It stores the value %d\n", *ptr_to_a);	// * = "what is pointed to by..."
	printf("The address of a is %d\n", &a);		// & = "the address of..." -- note that this is the same as ptr_to_a
}
