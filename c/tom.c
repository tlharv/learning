#include <stdio.h>

void main()
{
	int A = 5;
	int *ptrA;

	ptrA= &A; // sets the pointer equal to the address of A

	int arr[5]= { 2, 4, 8, 16, 32 };
	int *parr = NULL;

	// When you just call the array directly, you get the address
	// of that array.  So you assign the pointer to that address.
	parr = arr; // points to the array address

	printf("Value of arr[] is %x\n", arr);
	printf("Address of arr[] is %x\n", &arr);
	printf("Value of pointer *parr is %x\n", parr);

	for (int z=0; z<5; z++)
	{
		printf("Value %d = %d\n", z, *(parr + z));
	}
}
