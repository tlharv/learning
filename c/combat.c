#include <stdio.h>
#include <time.h>   // needed to create a random nunber generator seed
#include <stdlib.h> // needed for using random
#include <string.h> // needed for string operations

const int MaxNameLength = 10;

int RandStat(int upper, int lower)
{
	int z;
	z = (rand() % (upper-lower+1))+lower;
	return(z);
}

int dice(int num_of_dice, int faces)
{
	int z;
	int lower = 1;
	int upper = faces;
	int sum = 0;
	for (int i=0; i<num_of_dice; i++)
	{
		z = (rand() % (upper-lower+1))+lower;
		sum += z;
		//printf("Rolled a %d and the cumulative total is %d\n",z,sum);
	}
	return(sum);
}

void show_battlefield_size(int num_gg, int num_bg)
{
	// each person takes up a square 5 ft x 5 ft, or 25 sq feet.
	int totalAreaSQFT = (25*num_gg + 25*num_bg);
	printf("This will take place on a field of %d square feet or %f acre\n",totalAreaSQFT,(totalAreaSQFT/43560.0));
}

struct characteristics
{
	int STR, DEX, CON, INT, WIS, CHR;
	int HP;
};


struct combatant
{
	char NAME[20];
	struct characteristics stats;
};


void main(void)
{
	unsigned long i,j,k;
	int lower = 8;
	int upper = 15;
	int num_good_guys = 0;
	int num_bad_guys = 0;
	char thisname[MaxNameLength];

	srand((unsigned)time(NULL)); // for RNG seed

	printf("Enter the number of good guys: ");
	scanf("%d",&num_good_guys);
	printf("Enter the number of bad guys: ");
	scanf("%d",&num_bad_guys);

	struct combatant goodguys[num_good_guys];
	struct combatant badguys[num_bad_guys];

	show_battlefield_size(num_good_guys,num_bad_guys);

//	struct combatant *GGAptr; // Good Guy Array pointer
//	struct combatant *BGAptr; // Bad Guy Array pointer

//	GGAptr = goodguys; // pointer now points to array address
//	BGAptr = badguys;  // pointer now points to array address


	for (i=0; i<num_good_guys; i++)
	{
		sprintf(thisname, "GoodGuy_%d", i);
		strcpy(goodguys[i].NAME, thisname);
		goodguys[i].stats.HP = dice(2,10);
	}
	printf("%d good guy combatants created.\n",num_good_guys);

	for (j=0; j<num_bad_guys; j++)
	{
		sprintf(thisname, "BadGuy_%d", j);
		strcpy(badguys[j].NAME, thisname);
		badguys[j].stats.HP = dice(2,10);
	}
	printf("%d bad guy combatants created.\n",num_bad_guys);


/*
	for (i=0; i<num_good_guys; i++)
	{
		printf("%s has %d hit points\n",goodguys[i].NAME, goodguys[j].stats.HP);
	}

	for (j=0; j<num_bad_guys; j++)
	{
		printf("%s has %d hit points\n",badguys[j].NAME, badguys[j].stats.HP);
	}
*/

/*
	for (int k=0; k<num_bad_guys; k++)
	{
		sprintf(thisname, "BadGuy_%d", k);
		strcpy(*(BGAptr + k)->NAME, thisname);
		//strcpy(BGptr[i]->NAME, thisname);
		*(BGAptr+k)->stats.HP = dice(2,8);
	}
*/


	printf("Rolling d8.\n");
	printf("The total is %d\n",dice(44,8));
}
