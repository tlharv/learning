#include <stdio.h>

int main()
{
/*
	// Pointer to an integer
	int *p;

	// Pointer to an array of 5 integers 
	int (*ptr) [5];

	// Now let's make an array
	int arr[5] = { 10, 20, 30, 40, 50 };

	// Points to 0th element of the array arr[]
	p = arr;

	// points to the whole array arr[]
	ptr = &arr;


	printf("The size of arr[] os %d\n", sizeof(arr));
	printf("The size of *p is %d\n", sizeof(*p));
	printf("The size of *ptr is %d\n", sizeof(*ptr));


	printf("The second element in the array is %d\n", (ptr + 2));

*/

	/* an array with 5 elements */
	double balance[5] = {1000.0, 2.0, 3.4, 17.0, 50.0};
	double *p;
	int i;

	p = balance;

	/* output each array element's value */
	printf( "Array values using pointer\n");

	for ( i = 0; i < 5; i++ ) {
		printf("*(p + %d) : %f\n",  i, *(p + i) );
	}

	printf( "Array values using balance as address\n");

	for ( i = 0; i < 5; i++ ) {
		printf("*(balance + %d) : %f\n",  i, *(balance + i) );
	}

	return 0;


}
