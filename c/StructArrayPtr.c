#include <stdio.h>

// Student structure
struct student {
	char ID[15];
	char firstname[64];
	char lastname[64];
	float points;
}

// Create an array of three instances of this structure
struct student std[3];

// Create a pointer for this struct type
struct student *ptr = NULL;

// Assign this pointer to the array's starting memory location
ptr = std;

