# Lists are a clever way to keep related information together.
# Each list has its own name, like the name of a book.

Names = ['Marcus','James','Isaac','Nathan']

# Lists can hold any type of variable, including combinations!

MyList = ['Tom', 51, 'California', 173.2]

# You refer to list items by the INDEX, or position, of the data.
# The first item will be at index zero.  Names[0] is 'Marcus'.

print(MyList[2])  # Will print California

# Whenever you add, change or delete list elements, you use the list name.

MyList.append('Biscuit')

# When you create lists, you can put them all in a line or you can add to
# them on different lines using the list.append() function.
# ('append' means to add onto the end of an existing group of things)

States = ['CA', 'WA', 'VA', 'HI', 'OR', 'AZ', 'ID', 'WY', 'MT']

Players = ['Robot fighter']    # note the brackets to create the list
Players.append('Ratfolk monk') # we use () because append is a function
Players.append('High Elf Arcane Trickster')
Players.append('Human Cleric')

# To find out how many items are in a list, you use the len() function
party_count = len(Players)
print(party_count)   # Will print 4

# Remember that the indexes of list Players goes 0, 1, 2, 3

# Changing the value of a list item:
Players[0] = 'Mechanical Construct fighter'
