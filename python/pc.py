#!/usr/bin/python

# Before this can run, you must enter the following command in Terminal:
#    sudo pip3 install adafruit-blinka

import time
import board
from digitalio import DigitalInOut, Direction # contains classes to provide access to basic digital IO.

RCpin = board.D18  # This identifies the raspberry pi pin D18 in a variable we'll call RCpin

while True: # this sets up an infinite loop
	with DigitalInOut(RCpin) as rc:  # we rename the communication with RCpin as just rc, so it's easier to type
		reading = 0
		rc.direction = Direction.OUTPUT
		rc.value = False
		time.sleep(0.5)
		rc.direction = Direction.INPUT
		while rc.value is False:
			reading += 1
		print(reading)
