# Python stores data in different TYPES:
# Integers
A = 4
B = 17
C = 450000

# Floating-point numbers
D = 3.1415
E = 6.02
F = 22.0  # the decimal point makes it a float

# Strings
Name = 'Isaac'
Address = '22717 Echo Lake Rd'
City = 'Snohomish'

