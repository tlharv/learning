# Use the IF statement to tell your program how to make decisions

Driving_Age = 16

MyAge = int(input("Enter your age: "))

if (MyAge >= Driving_Age):
	print("You're old enough to drive!")
else:
	print("You're not old enough to drive yet.")


# Python also has a type called Boolean which holds either True or False.
School_night = True
Weekend = False
Movie_night = not School_night    # use NOT to make it the opposite value


# You can use these Boolean values directly in an IF statement
if(Weekend):
	print("Let's watch a movie!")
else:
	print("Boooo hoooooo")


# Python also has a way to make lots of related decisions
myRoll = int(input("Roll a d4 and tell me what you got: "))

if (myRoll == 1):
	monster = "Kobold"
elif (myRoll == 2):
	monster = "Giant fly"
elif (myRoll == 3):
	monster = "Zombie"
else:
	monster = "Maniac barbarian"

print(monster)

# You can combine conditions in your IF statement as well.
# In this example, everything in the IF parentheses must be true:
if (myRoll == 2 and MyAge == 51 and Weekend):
	print("Dad is fighting a giant fly this weekend!")

# You can also make it so that any one of them is true
if (myRoll == 4 or MyAge == 12):
	print("This is true")
