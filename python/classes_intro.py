import random # not needed to make a class, but I use it in an examnple

# This example shows the very basic use of a class in Python.
# Classes are used in the structure of your program to make repeatable building blocks.

# For example, every person on the planet has a name, hair color, eye color, and age.
# So first we will look at how to do it without classes:

Marcus_name = 'Marcus'
Marcus_hair = 'Brown'
Marcus_eyes = 'Brown'
Marcus_age = 14

Isaac_name = 'Isaac'
Isaac_hair = 'Ginger'
Isaac_eyes = 'Blue'
Isaac_age = 13

# You can see that if you have a lot of people to define in your program, you'd have a lot of typing to do!
# So what we do is create a generic 'person' template, like a blank index card with spaces available for
# name, hair, eyes and age.

class person():
    def __init__(self,name,hair,eyes,age):
        self.name = name
        self.hair = hair
        self.eyes = eyes
        self.age = age

# Think of this class as a stack of those index cards, ready to be used.
# Let's fill out cards for Marcus & Isaac.
Marcus = person('Marcus','brown','brown',14)
Isaac = person('Isaac','ginger','blue',13)

# In this case, Marcus and Isaac are INSTANCES of the class person.  
# You could also say they are OBJECTS of that class.
