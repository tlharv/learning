#!/usr/bin/python

# Before this can run, you must enter the following command in Terminal:
#    sudo pip3 install adafruit-blinka

import time
import board
from digitalio import DigitalInOut, Direction # contains classes to provide access to basic digital IO.

# RCpin = board.D18  # This identifies the raspberry pi pin D18 in a variable we'll call RCpin
FETpin = board.D17

while True:
	with DigitalInOut(FETpin) as fp:
		fp.direction = Direction.OUTPUT
		fp.value = False
		time.sleep(0.5)
		fp.value = True
		time.sleep(1.0)
