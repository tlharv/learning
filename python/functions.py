# Example 1: a function with no argument and no return
def simple():
    print("hi")

# You call the function with empty parentheses
simple()

# =======================================

# Example 2: a function with one or more arguments but no return
def do_something(a, b):
    print(a * b)

# You call this function by passing it arguments that become a and b used in the function
do_something(2, 4)

# =======================================

# Example 3: a function with no argument but it returns a value to you
def chickenpoop():
    return 3.14159275

# because this function returns a value, in the main part of the program you have to
# assign it to a variable.
X = chickenpoop()
print(X)

# =======================================

# Example 4: a function with both arguments and a return
def Catbox(i, j):
    answer = (i * j) ** (i/j)
    return answer

Y = Catbox(5,10)
print(Y)


