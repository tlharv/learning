# In this example, we're going to make 1000 clones each having random values of hair, eyes, and age.

import random

class person():
    def __init__(self,name,hair,eyes,age):
        self.name = name
        self.hair = hair
        self.eyes = eyes
        self.age = age

# Let's create lists for the random hair and eye color options.
hair = ['blond','strawberry blond','ginger','brown','dark brown','raven','grey','white','bald']
eyes = ['blue','brown','black','green','hazel','pink','blue-grey','grey']

# We'll assume the age of the clone is between 1 and 100.

# Now we'll create the loop
for x in range(1,1000):
    haircolor = random.choice(hair)
    eyecolor = random.choice(eyes)
    age = random.randint(1,100)
    name = 'clone_' + str(x)
    clone = person(name,haircolor,eyecolor,age)
    print(clone.name, clone.hair, clone.eyes, clone.age)
