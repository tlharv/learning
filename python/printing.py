# Most of the time, you can print out anything you want.
# It's a handy way to check the values of variables while your code is running.

print(20)		# printing a literal value
print(3.14159)		# printing a literal float value
print('Thomas')		# printing a literal string value

# Printing the values of variables in your code
A = 4
B = 3.27

print(A)
print(A * B)	# You can do math inside the print statement!

First_Name = "Thunder"
Second_Name = "Paw"

print(First_Name)  # Prints "Thunder"
print(First_Name + Second_Name)	# prints "ThunderPaw"

# To mix string, integer and float variables in your print statement, you 
# first need to convert all non-strings to strings by using the str() function.
# What you want to convert becomes the argument to the str() function
Name1 = str(First_Name)
Name2 = str(Second_Name)
print("The clan leader is " + Name1 + Name2)

# You can also convert the non-strings to strings inside the print statement
# but it looks a little messy until you know what you're doing:
print("The clan leader is " + str(First_Name) + str(Second_Name))
