# This example shows the random library.

# DEFINITIONS PART OF THE PROGRAM

# First, pull in a library that gives our program the ability to create random numbers
import random

# Create a function that performs a simple six-sided dice roll.
def d6():  
    roll = random.randint(1,6) # pick a number between 1 and 6
    return roll # This sends the roll value back to the main program



# MAIN PART OF THE PROGRAM (WHAT GETS EXECUTED)
print('Hello!  Welcome to the dice-rolling program.')
MyRoll = d6()
print(MyRoll)
print("That's it.  Goodbye!")