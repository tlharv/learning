# Getting a response from the user always takes this form:
#         Variable Name = input("Description")

# Ask the user to enter a string, and print it back to them
MyName = input("Please enter your name: ")
print("Hi " + MyName)


# Now ask the user to enter an integer that we'll keep in variable x
x = int(input("Enter a whole number: "))

# Now ask the user to enter a float that we'll keep in variable y
y = float(input("Enter a floating-point number: "))

