#!/usr/bin/python

# Before this can run, you must enter the following command in Terminal:
#    sudo pip3 install adafruit-blinka

import time
import board
from digitalio import DigitalInOut, Direction # contains classes to provide access to basic digital IO.

RCpin = board.D18
FETpin = board.D17

def LightLevel(pin):
	with DigitalInOut(pin) as rc:
		reading = 0
		rc.direction = Direction.OUTPUT
		rc.value = False
		time.sleep(0.5)
		rc.direction = Direction.INPUT
		while rc.value is False:
			reading += 1
		return(reading)



def LightSwitch(pin, signal):
	with DigitalInOut(FETpin) as fp:          # Rename the communication with this pin "fp" for "FET pin"
		fp.direction = Direction.OUTPUT   # Define the direction as OUTPUT, meaning we send a signal out through this pin
		fp.value = signal                 # This is the signal we actually send through the pin.


while True:
	LL = LightLevel(RCpin)
	if (LL > 800):
		LightSwitch(FETpin, True)
	else:
		LightSwitch(FETpin, False)


