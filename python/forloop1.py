# Python does For loops in a weird way.
# The looping variable is x.
# The looping begins with x = 1.
# The looping ends when x = 6, and then it'll IMMEDIATELY end the loop.
# That is why you only see it print up to 5.

# print numbers from 1 to 5
for x in range(1,6):
	print(x)
